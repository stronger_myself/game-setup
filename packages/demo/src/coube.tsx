import React, { FunctionComponent, useRef, useEffect } from 'react'
import { useFrame, useThree } from 'react-three-fiber'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'

interface Props {}

export const Coube: FunctionComponent<Props> = (props) => {
    const ref = useRef<any>()
    const { gl, scene, camera } = useThree()
    useEffect(() => {
        scene.background = new THREE.Color( 0xcccccc )
        scene.fog = new THREE.FogExp2( 0xcccccc, 0.002 )
    }, [])
    
    useFrame(({ gl, scene, camera }) => {
        // camera.updateMatrix()
        // camera.lookAt(ref.current.position)
        // ref.current.rotation.x = ref.current.rotation.y += 0.01
        // gl.render(scene, camera)
    })
    return (
        <mesh ref={ref}>
            <boxBufferGeometry attach="geometry" args={[2, 1, 1]} />
            <meshNormalMaterial attach="material" />
        </mesh>
    )
}

Coube.displayName = 'Coube'
