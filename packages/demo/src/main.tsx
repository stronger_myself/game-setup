import React, { FunctionComponent } from 'react'
import { Canvas } from 'react-three-fiber'
import { Player } from './player'
import { World } from './world'
import { useWorld } from "physics";

interface Props {}

export const Main: FunctionComponent<Props> = (props) => {
    return (
        <Canvas style={{ width: '100vw', height: '100vh' }}>
            <World>
                <Player/>
            </World>
        </Canvas>
    )
}

Main.displayName = 'Main'
