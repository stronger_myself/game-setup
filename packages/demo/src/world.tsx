import React, { useEffect, useRef } from 'react'
import * as THREE from 'three'

let colors = []
let color = new THREE.Color()
var colorBuffer = new THREE.PlaneBufferGeometry( 2000, 2000, 100, 100 )
var colorPos = colorBuffer.attributes.position;
for ( let i = 0, l = colorPos.count; i < l; i ++ ) {
    color.setHSL( Math.random() * 0.3 + 0.5, 0.75, Math.random() * 0.25 + 0.75 )
    colors.push( color.r, color.g, color.b )
}

const Box = () => {
    const box = useRef<THREE.PlaneBufferGeometry>()   

    useEffect(() => {
        box.current.addAttribute('color',new THREE.Float32BufferAttribute(colors, 3))
    }, [])

    return (
        <mesh
            name="box"
            position={new THREE.Vector3(
                Math.floor( Math.random() * 20 - 10 ) * 20,
                Math.floor( Math.random() * 20 ) * 20 + 11,
                Math.floor( Math.random() * 20 - 10 ) * 20,
            )}
        >
            <boxBufferGeometry
                ref={box}
                attach="geometry"
                args={[20, 20, 20]}
            />
            <meshPhongMaterial
                attach="material"
                color={new THREE.Color(Math.random() * 0.2 + 0.5, 0.75, Math.random() * 0.25 + 0.75)}
                specular={new THREE.Color(0xffffff)}
                flatShading={true}
                vertexColors={THREE.VertexColors}
            />
        </mesh>
    )
}

const Floor = () => {
    const ref = useRef<THREE.Mesh>()    
    const plane = useRef<THREE.PlaneBufferGeometry>()    
    
    useEffect(() => {
        plane.current.rotateX(-Math.PI / 2)
        let position = plane.current.attributes.position as THREE.BufferAttribute
        let vertex = new THREE.Vector3()
        for ( let i = 0, l = position.count; i < l; i ++ ) {
            vertex.fromBufferAttribute(position, i)
            vertex.x += Math.random() * 20 - 10
            vertex.y += Math.random() * 2
            vertex.z += Math.random() * 20 - 10
            position.setXYZ( i, vertex.x, vertex.y, vertex.z )
            plane.current.addAttribute('color',new THREE.Float32BufferAttribute(colors, 3))
        }
    }, [])

    return (
        <mesh ref={ref}>
            <planeBufferGeometry
                attach="geometry"
                ref={plane}
                args={[2000, 2000, 100, 100]}
            />
            <meshBasicMaterial
                attach="material"
                vertexColors={THREE.VertexColors}
            />
        </mesh>
    )
}

export const World = ({ children }) => {
    return (
        <group name="world">
            <hemisphereLight
                skyColor={new THREE.Color(0xeeeeff)}
                groundColor={new THREE.Color(0x777788)}
                intensity={0.75}
                position={[0.5, 1, 0.75]}
            />
            <Floor/>
            {new Array(500).fill(0).map((el, i) => (
                <Box key={i}/>
            ))}
            {children}
        </group>
    )
}

World.displayName = 'World'
