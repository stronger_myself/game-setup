import React, { FunctionComponent, useEffect, useRef } from 'react'
import { useThree } from 'react-three-fiber'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'

interface Props {}

export const Camera: FunctionComponent<Props> = (props) => {
    const ref = useRef<THREE.PerspectiveCamera>()
    const { gl, setDefaultCamera } = useThree()
    
    useEffect(() => {
        setDefaultCamera(ref.current)
        new OrbitControls(ref.current, gl.domElement)
        // camera.position.set(1000, 500, 1000)
        ref.current.lookAt(0, 200, 0)
    }, [])

    return (
        <perspectiveCamera
            ref={ref}
            position={[750, 500, 750]}
            args={[45, window.innerWidth / window.innerHeight, 1, 3000]}
        />
    )
}

Camera.displayName = 'Camera'
   