import React, { FunctionComponent, useEffect, useRef, useState  } from 'react'
import { useThree, useFrame } from 'react-three-fiber'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import * as THREE from 'three'
import * as CANNON from 'cannon'
import { useWorld } from 'physics'

const states = ['Idle', 'Walking', 'Running', 'Dance', 'Death', 'Sitting', 'Standing']
const emotes = ['Jump', 'Yes', 'No', 'Wave', 'Punch', 'ThumbsUp']

interface Props {
    url?, state?, emotes?
    onLoad?: (ref: CANNON.Body) => void
}

export const Hero: FunctionComponent<Props> = ({
    url = require('../public/models/RobotExpressive.glb'),
    state,
    onLoad = (ref: CANNON.Body) => {}
}) => {
    const clock = useRef<THREE.Clock>()
    const mixer = useRef<THREE.AnimationMixer>()
    const model = useRef<THREE.Scene>()
    const hero = useRef<THREE.Object3D>();
    const actions = useRef<{[key: string]: THREE.AnimationAction}>({})
    const activeAnim = useRef<THREE.AnimationAction>()
    const [loaded, setLoaded] = useState(false)

    const body = useRef<CANNON.Body>()
    let world = useWorld()

    useEffect(() => {
        clock.current = new THREE.Clock()
        var loader = new GLTFLoader()
        loader.load(url, (gltf) => {
            model.current = gltf.scene
            model.current.scale.x = 7
            model.current.scale.y = 7
            model.current.scale.z = 7
            mixer.current = new THREE.AnimationMixer(model.current)
            gltf.animations.forEach(anim => {
                actions.current[anim.name] = mixer.current.clipAction(anim)
                if (emotes.indexOf(anim.name) >= 0 || states.indexOf(anim.name) >= 4 ) {
                    actions.current[anim.name].clampWhenFinished = true
                    actions.current[anim.name].loop = THREE.LoopOnce
                }
                if (anim.name === 'Running') {
                    actions.current['FastRunning'] = mixer.current.clipAction(anim)
                    actions.current['FastRunning'].timeScale = 1.5
                }
            })
            activeAnim.current = actions.current['Idle']
            activeAnim.current.play()
            setLoaded(true)
        }, undefined, (e) => {
            console.error(e)
        })
    }, [])
    
    useEffect(() => {
        let act = actions.current[state]
        if (!act) return
        activeAnim.current
            .fadeOut(0.5)
        activeAnim.current = act
        activeAnim.current
            .reset()
            .setEffectiveTimeScale(1)
            .setEffectiveWeight(1)
            .fadeIn(0.5)
            .play()
    }, [state])

    useEffect(() => {
        if (loaded) {
            const shape = new CANNON.Cylinder(3.5, 3.5, 7, 10)
            // const shape = new CANNON.Box(new CANNON.Vec3(100/2, 100/2, 100/2));
            body.current = new CANNON.Body({ mass: 10 });
            body.current.addShape(shape);
            world.addBody(body.current);
            onLoad(body.current)
        }
    }, [loaded])

    useFrame(() => {
        var dt = clock.current.getDelta()
        if (mixer.current) mixer.current.update(dt)
    })

    return (
        <>
            {model.current && (
                <primitive ref={hero} object={model.current} />
            )}
        </>
    )
}

Hero.displayName = 'Hero'
