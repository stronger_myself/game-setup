import React, { FunctionComponent } from 'react'
import { Canvas } from 'react-three-fiber'
import { World as PWordld, Square } from 'physics'
import { World } from './world'
import { Camera } from './camera'
import { Player } from './player'

interface Props {}

export const Main: FunctionComponent<Props> = (props) => {
    return (
        <Canvas style={{ width: '100vw', height: '100vh' }} >
            <PWordld>
                <World>
                <Square position={[0,0,200]} mass={10000000000} size={[50, 50, 50]} />
                <Square position={[0,0,500]}  velocity={[0,-5,0]} mass={10} size={[50, 50, 50]} />
                <Square position={[400,0,400]}  velocity={[-5,0,0]} mass={10} size={[50, 50, 50]} />
                <Square position={[500,0,500]} velocity={[5,0,0]} size={[50, 50, 50]} />
                    {/* <Camera/> */}
                    <Player getRef={(ref) => console.log(ref)}/>
                    {/* <Hero url={require('../public/models/RobotExpressive.glb')}/> */}
                    {/* <Hero
                        modelUrl={require('../public/models/ty.fbx')}
                        animUrl={require('../public/actions/Run Forward.fbx')}
                    /> */}
                </World>
            </PWordld>
        </Canvas>
    )
}

Main.displayName = 'Main'
