import React, { FunctionComponent, useEffect, useRef, useState } from 'react'
import { Hero } from './hero'
import { useFrame, useThree } from 'react-three-fiber'
import * as CANNON from 'cannon'

interface Props {
    getRef?
}

export const Player: FunctionComponent<Props> = ({ 
    getRef = (ref) => {}
 }) => {
    const groupRef = useRef<THREE.Group>()
    const cameraRef = useRef<THREE.PerspectiveCamera>()
    const [heroState, setHeroState] = useState()
    const [forward, setforward] = useState(false)
    const [backward, setbackward] = useState(false)
    const [left, setleft] = useState(false)
    const [right, setright] = useState(false)
    const [jump, setjump] = useState(false)
    const [pos, setPos] = useState(0)
    const [rotatePos, setRotatePos] = useState(0)

    const body = useRef<CANNON.Body>()

    const { setDefaultCamera } = useThree()
    
    useEffect(() => {
        setDefaultCamera(cameraRef.current)
        cameraRef.current.lookAt(groupRef.current.position)
        document.addEventListener('keydown', onKeyDown)
        document.addEventListener('keyup', onKeyUp)
        getRef(groupRef)
    }, [])

    useEffect(() => {
        if (forward) {
            setHeroState('Running')
        } else {
            setHeroState('Idle')
        }
    }, [forward])

    const onKeyDown = (event) => {
        switch (event.keyCode) {
            case 38: // up
            case 87: // w
                setPos(2)
                setforward(true)
                break;
            case 37: // left
            case 65: // a
                setRotatePos(0.05)
                // setleft(true)
                break;
            case 40: // down
            case 83: // s
                // setRotatePos((d) => Math.PI)
                // setbackward(true)
                break;
            case 39: // right
            case 68: // ds
                setRotatePos(-0.05)
                // setright(true)
                break;
            case 32: // space
                setjump(false)
                break;
        }
    }

    const onKeyUp = (event) => {
        switch (event.keyCode) {
            case 38: // up
            case 87: // w
                setforward(false)
                break;
            case 37: // left
            case 65: // a
                setRotatePos(0)
                // setleft(false)
                break;
            case 40: // down
            case 83: // s
                setRotatePos(0)
                setbackward(false)
                break;
            case 39: // right
            case 68: // d
                setRotatePos(0)
                // setright(false)
                break;
        }
    }

    useFrame(() => {
        if (groupRef.current) {
            groupRef.current.rotateY(rotatePos)
            if (heroState === 'Running') {
                groupRef.current.translateZ(pos)
            }
        }
        // if (body.current) {
        //     body.current.position.copy(groupRef.current.position)
        //     body.current.quaternion.copy(groupRef.current.quaternion)
        //     // groupRef.current.position.copy(body.current.position);
        //     // groupRef.current.quaternion.copy(body.current.quaternion);
        // }
    })

    return (
        <group ref={groupRef}>
            <Hero state={heroState} onLoad={(ref) => body.current = ref}/>
            <perspectiveCamera
                ref={cameraRef}
                position={[10, 50, -120]}
                args={[45, window.innerWidth / window.innerHeight, 1, 3000]}
            />
        </group>
    )
}

Player.displayName = 'Player'
