import React, { FunctionComponent, useEffect } from 'react'
import { useThree } from 'react-three-fiber'
import * as THREE from 'three'

interface Props {}

export const World: FunctionComponent<Props> = ({
    children
}) => {
    return (
        <group name="world">
            <hemisphereLight
                args={[0xffffff, 0x444444]}
                position={[0, 20, 0]}
            />
            <gridHelper
                args={[1000, 10]}
            />
            {children}
        </group>
    )
}

World.displayName = 'World'
