import React, {FunctionComponent, useRef, useEffect, useContext} from 'react';
import {useFrame} from "react-three-fiber";
import * as CANNON from 'cannon';
import useWorld from "../services/useWorld";

interface Props {
    position: number[],
    size?: number[],
    velocity?: number[],
    mass?: number,
}

export const Square: FunctionComponent<Props> = ({position, velocity, mass, size}) => {
    const mesh = useRef();
    let world = useWorld();
    let body = useRef<CANNON.Body>();
    if (!size) {
        size = [1, 1, 1];
    }
    if (!mass) {
        mass = 1;
    }
    // if (!velocity) {
    //     velocity = [0,0,0];
    // }
    const [x, y ,z] = position;
    const [sx, sy, sz] = size;
    
    useEffect(() => {
        const shape = new CANNON.Box(new CANNON.Vec3(sx/2, sy/2, sz/2));
        body.current = new CANNON.Body({ mass });
        body.current.addShape(shape);
        body.current.position.set(x,y,z);
        body.current.linearDamping = 0.3;
        body.current.angularDamping = 0.3;
        if (velocity) {
            const [xv, yv, zv] = velocity;
            body.current.velocity.set(xv,yv,zv);
        }
        world.addBody(body.current);
    }, []);
    useFrame(() => {
        if (body.current) {
            mesh.current.position.copy(body.current.position);
            mesh.current.quaternion.copy(body.current.quaternion);
        }
    });

    return (
        <mesh
            ref={mesh}
            onClick={e => console.log('click')}
            onPointerOver={e => console.log('hover')}
            onPointerOut={e => console.log('unhover')}
            >
            <boxBufferGeometry attach="geometry" args={[size[0], size[1], size[2]]} />
            <meshNormalMaterial attach="material" />
        </mesh>
    )
}
