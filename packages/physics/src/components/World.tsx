import React, {FunctionComponent, useRef, useEffect, useContext, useState} from 'react';
import {WorldContext} from "../services/WorldContext";
import * as THREE from "three";

import * as CANNON from 'cannon';
import {useFrame} from "react-three-fiber";
import {Square} from "./Square";
import io from '../../../world/src/socket.io.js'
const socket = io('http://worond.com:80');

socket.on('connection', function(res) {
    console.log(res);

    socket.emit('events', { test: 'test' }, response =>
        console.log('Identity:', response),
    );
});
socket.on('events', function(data) {
    //противник ходит
    console.log('event', data);
});
socket.on('exception', function(data) {
    console.log('event', data);
});
socket.on('disconnect', function() {
    console.log('Disconnected');
});

interface Props {}

const Floor = () => {
    const ref = useRef<THREE.Mesh>();
    const plane = useRef<THREE.PlaneBufferGeometry>();
    let texture = useRef<THREE.Texture>();
    // texture.wrapS = texture.wrapT = THREE.RepeatWrapping;



    useEffect(() => {
        plane.current.rotateX(-Math.PI / 2);
        let position = plane.current.attributes.position as THREE.BufferAttribute;
        let vertex = new THREE.Vector3();
        for ( let i = 0, l = position.count; i < l; i ++ ) {
            vertex.fromBufferAttribute(position, i);
            position.setXYZ( i, vertex.x, vertex.y, vertex.z );
        }
        texture.current = new THREE.TextureLoader().load(require("../../../../static/ground.jpg"));
        texture.current.repeat.set(1,1);
    }, []);
    return (
        <mesh ref={ref}>
            <planeBufferGeometry
                attach="geometry"
                ref={plane}
                args={[2000, 2000, 100, 100]}
            />
            <meshPhongMaterial
                attach="material"
                specular={new THREE.Color(0x000000)}
                color={new THREE.Color(0x77ff77)}
                map={texture.current}
                side={THREE.DoubleSide}
            />
        </mesh>
    )
};


export const World: FunctionComponent<Props> = ({children}) => {
    let [boxes, changeBoxes] = useState([]);
    useEffect(() => {
        fetch("http://worond.com/map")
        .then(data => data.json()).then(data => {
        let new_boxes = [];
        for(let i=0;i<data["boxes"].length;i++){
            let box = data["boxes"][i];
            new_boxes.push(<Square position={[box.x, box.y, box.z]} size={[20,20,20]}/>)
        }
        changeBoxes(new_boxes)
    });
        },
        [])
    const step = 1 / 60;
    const world = new CANNON.World();
    world.gravity.set(0, 0, 0);
    world.broadphase = new CANNON.NaiveBroadphase();
    world.solver.iterations = 10;
    useFrame(() => {
        world.step(step);
    });

    return (
        <group name="world">
            <WorldContext.Provider value={world}>
                <hemisphereLight
                    skyColor={new THREE.Color(0xeeeeff)}
                    groundColor={new THREE.Color(0x777788)}
                    intensity={0.75}
                    position={[0.5, 1, 0.75]}
                />
                <Floor/>
                {boxes}
                {children}
            </WorldContext.Provider>
        </group>
    )
}
