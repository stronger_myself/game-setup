import React, {FunctionComponent} from 'react';
import {Canvas} from 'react-three-fiber';
import {Square} from  "./components/Square";
import {World} from "./components/World";

interface Props {}

export const Main: FunctionComponent<Props> = (props) => {
    return (
        <Canvas style={{ width: '100vw', height: '100vh' }}>
            <World>
                <Square position={[0,0,0]}  velocity={[0,0,0]} mass={10000000000} />
                <Square position={[0.5,5,0]}  velocity={[0,-5,0]} mass={10} />
                <Square position={[5,0.5,0]}  velocity={[-5,0,0]} mass={10} />
                <Square position={[-5,0.5,0.5]} velocity={[5,0,0]} />
            </World>
        </Canvas>
    )
}

Main.displayName = 'Main';
