import * as React from "react";
import * as CANNON from 'cannon';

export const WorldContext = React.createContext<CANNON.World>({});
