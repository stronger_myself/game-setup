import {useContext} from "react";
import {WorldContext} from "./WorldContext";

const useWorld = () => {
    return useContext(WorldContext);
};

export default useWorld;
