import React, { FunctionComponent } from 'react'
import { Canvas } from 'react-three-fiber'

interface Props {}

export const Main: FunctionComponent<Props> = (props) => {
    
    return (
        <Canvas style={{ width: '100vw', height: '100vh' }}>
            <></>
        </Canvas>
    )
}

Main.displayName = 'Main'
