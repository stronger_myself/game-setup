import {
  ConnectedSocket,
  MessageBody, OnGatewayConnection,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import  * as uuidv4 from 'uuid/v4';

@WebSocketGateway()
export class EventsGateway implements OnGatewayConnection{
  @WebSocketServer()
  server: Server;

  handleConnection(@ConnectedSocket() client: Socket) {
    const uuid = uuidv4();
    client.emit('connection', uuid);
  }

  @SubscribeMessage('events')
  async identity(@MessageBody() data: number, @ConnectedSocket() client: Socket): Promise<number> {
    client.broadcast.emit('events',data);
    return data;
  }
}
