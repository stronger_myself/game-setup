import { Controller, Get, Header } from '@nestjs/common';
import * as mapData from './map.json'

interface Box {
    x: number;
    y: number;
    z: number;
}

@Controller('map')
export class MapController {
    @Get()
    @Header('Content-Type', 'application/json; charset=utf-8')
    getMap() {
        return mapData;
    }

    @Get('generate')
    generateMap() {
        let boxes: Box[] = [];
        for(let i = 0; i < 300; i++) {
            boxes.push({
                x: Math.floor(Math.random() * 20 - 10) * 20,
                y: Math.floor(Math.random() * 20) * 20 + 11,
                z: Math.floor(Math.random() * 20 - 10) * 20,
            })
        }
        return {boxes:boxes}
    }
}
