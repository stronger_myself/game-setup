import React, {FunctionComponent, useEffect, useRef} from 'react'
import { Canvas } from 'react-three-fiber'
import {World as WorldPhysic} from 'physics'
import {Player} from "hero";

interface Props {}

export const Main: FunctionComponent<Props> = (props) => {
    const ref = useRef();
    useEffect(() => {
        console.log(ref.current)
    },[]);
    return (
        <Canvas style={{ width: '100vw', height: '100vh' }}>
            <WorldPhysic>
                <Player/>
            </WorldPhysic>
        </Canvas>
    )
}

Main.displayName = 'Main'
