import React, { FunctionComponent, useRef, useEffect } from 'react'
import { useThree, useFrame } from 'react-three-fiber'
import * as THREE from 'three'
import { PointerLockControls } from 'three/examples/jsm/controls/PointerLockControls'

interface Props {}

export const Player: FunctionComponent<Props> = (props) => {
    const ref = useRef<THREE.PerspectiveCamera>()
    const controls = useRef<PointerLockControls>()
    const moveState = useRef({ forward: false, backward: false, left: false, right: false, jump: true })
    const { setDefaultCamera, gl, scene } = useThree()
    const boxes = useRef<THREE.Object3D[]>()
    const raycaster = useRef<THREE.Raycaster>()
    const prevTime = useRef<number>()
    const velocity = useRef<THREE.Vector3>() 
    const direction = useRef<THREE.Vector3>() 

    useEffect(() => {
        setDefaultCamera(ref.current)
        
        boxes.current = scene.children.find(el => el.name === 'world').children.filter(el => el.name === 'box')
        raycaster.current = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3( 0, - 1, 0 ), 0, 10 );

        controls.current = new PointerLockControls(ref.current)
        scene.add(controls.current.getObject())
        gl.domElement.addEventListener('click', () => {
            controls.current.lock()
        })
        prevTime.current = performance.now()
        velocity.current = new THREE.Vector3()
        direction.current = new THREE.Vector3()
        document.addEventListener('keydown', (event) => {
            switch ( event.keyCode ) {
                case 38: // up
                case 87: // w
                    moveState.current.forward = true;
                    break;
                case 37: // left
                case 65: // a
                    moveState.current.left = true;
                    break;
                case 40: // down
                case 83: // s
                    moveState.current.backward = true;
                    break;
                case 39: // right
                case 68: // d
                    moveState.current.right = true;
                    break;
                case 32: // space
                    if ( moveState.current.jump === true ) velocity.current.y += 350;
                    moveState.current.jump = false;
                    break;
            }
        })
        document.addEventListener('keyup', (event) => {
            switch ( event.keyCode ) {
                case 38: // up
                case 87: // w
                    moveState.current.forward = false;
                    break;
                case 37: // left
                case 65: // a
                    moveState.current.left = false;
                    break;
                case 40: // down
                case 83: // s
                    moveState.current.backward = false;
                    break;
                case 39: // right
                case 68: // d
                    moveState.current.right = false;
                    break;
            }
        })
    }, [])

    useFrame(({ camera }) => {

        raycaster.current.ray.origin.copy(controls.current.getObject().position)
        raycaster.current.ray.origin.y -= 10
        let intersections = raycaster.current.intersectObjects(boxes.current)
        let onObject = intersections.length > 0

        let time = performance.now()
        let delta = ( time - prevTime.current ) / 1000
        velocity.current.x -= velocity.current.x * 10.0 * delta
        velocity.current.z -= velocity.current.z * 10.0 * delta
        velocity.current.y -= 9.8 * 100.0 * delta // 100.0 = mass
        direction.current.z = Number( moveState.current.forward ) - Number( moveState.current.backward )
        direction.current.x = Number( moveState.current.right ) - Number( moveState.current.left )
        // direction.current.normalize() // this ensures consistent movements in all directions
        if ( moveState.current.forward || moveState.current.backward ) velocity.current.z -= direction.current.z * 400.0 * delta
        if ( moveState.current.left || moveState.current.right ) velocity.current.x -= direction.current.x * 400.0 * delta
        if ( onObject === true ) {
            velocity.current.y = Math.max(0, velocity.current.y)
            moveState.current.jump = true
        }
        controls.current.moveRight( - velocity.current.x * delta )
        controls.current.moveForward( - velocity.current.z * delta )
        controls.current.getObject().position.y += ( velocity.current.y * delta ) // new behavior
        if ( controls.current.getObject().position.y < 10 ) {
            velocity.current.y = 0
            controls.current.getObject().position.y = 10
            moveState.current.jump = true
        }
        prevTime.current = time
    })

    return (
        <perspectiveCamera
            ref={ref}
            position={[0, 10, 20]}
            args={[75, window.innerWidth / window.innerHeight, 1, 1000]}
        />
    )
}

Player.displayName = 'Player'
