import io from './socket.io.js'
const socket = io('http://worond.com:80');
socket.on('connection', function(res) {
    console.log(res);

    socket.emit('events', { test: 'test' }, response =>
        console.log('Identity:', response),
    );
});
socket.on('events', function(data) {
    console.log('event', data);
});
socket.on('exception', function(data) {
    console.log('event', data);
});
socket.on('disconnect', function() {
    console.log('Disconnected');
});