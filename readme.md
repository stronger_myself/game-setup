## Game workflow

Monorepa based on ```lerna``` and ```yarn workspaces```

#### Install

```sh
npm install -g lerna
yarn bootstrap
```

##### Add new package

```sh
cp ./package/sample/**/* ./package/newPackage
rename name in ./package/newPackage/package.json
yarn bootstrap
```

##### Scripts
- ```yarn bootstrap``` install all dependencies and symblink
- ```yarn ls``` list packages
- ```yarn dev``` run development on all packages
- ```yarn dev --scope packageName``` run development on define package
- ```yarn build``` run build on all packages
- ```yarn build --scope packageName``` run build on define package
- ```yarn clean``` clean dependencies packages (does not touch root)

##### Git hooks
- ```"post-merge": "yarn bootstrap"``` - after all merge run yarn bootstrap

##### Dev dependencies
- ```node```
- ```yarn```
- ```typescript```
- ```lerna```
- ```parsel-bundler```

##### Dependencies
- ```react```
- ```react-dom```
- ```three```
- ```react-three-fiber```

## Hackathon 26.10.2019

hero

![](/example/hero.gif)

player

![](/example/player.gif)

physics

![](/example/physics.gif)

